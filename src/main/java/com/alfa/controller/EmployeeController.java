package com.alfa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alfa.request.EmployeeRequest;
import com.alfa.response.EmployeeResponse;
import com.alfa.service.EmployeeService;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    @Autowired
    EmployeeService service;

    @PostMapping()
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest requestDto){
        return service.createEmployee(requestDto);
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployee(){
        return service.getAllEmployee();
    }

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Integer id){
        return service.getEmployeeById(id);
    }

    @PutMapping("/{id}")
    public EmployeeResponse updateEmployee(@PathVariable Integer id, @RequestBody EmployeeRequest requestDto){
        return service.updateEmployee(id,requestDto);
    }

    @DeleteMapping("/{id}")
    public String deleteEmployee(@PathVariable Integer id){
        return service.deleteEmployeeById(id);
    }
}
