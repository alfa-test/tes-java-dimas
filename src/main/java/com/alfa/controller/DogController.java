package com.alfa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alfa.response.DogResponse;
import com.alfa.service.DogService;

@RestController
@RequestMapping("/api/v1/dogs")
public class DogController {
	
	@Autowired
	DogService service;
	
	@GetMapping
	public List<DogResponse> getDogs() {
		return service.getDogs();
	}
	
}
