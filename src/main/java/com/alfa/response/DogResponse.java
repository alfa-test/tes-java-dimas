package com.alfa.response;

import java.util.List;

public class DogResponse {
	
	private String breed;
	
	private List<DogResponse> sub_breed;

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public List<DogResponse> getSub_breed() {
		return sub_breed;
	}

	public void setSub_breed(List<DogResponse> sub_breed) {
		this.sub_breed = sub_breed;
	}
	
}
