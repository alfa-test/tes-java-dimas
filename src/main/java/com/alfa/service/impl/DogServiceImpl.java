package com.alfa.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alfa.response.DogResponse;
import com.alfa.service.DogService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DogServiceImpl implements DogService{

	@Override
	public List<DogResponse> getDogs() {
        
	       List<DogResponse> response = new ArrayList<DogResponse>();

	    try {

	        String url = "https://dog.ceo/api/breeds/list/all";

	        RestTemplate restTemplate = new RestTemplate();

	        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);

	        ObjectMapper mapper = new ObjectMapper();
	        Map<String, Map<String, List<String>>> map = mapper.readValue(result.getBody().toString(), Map.class);
	        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("message")));
	        Map<String, List<String>> innerMap = map.get("message");

	          for (Entry<String, List<String>> entry : innerMap.entrySet()) {
	              DogResponse dog = new DogResponse();
	              dog.setBreed(entry.getKey());
	              
	              List<String> subBreeds = entry.getValue();
	              
	              List<DogResponse> subBred = new ArrayList<DogResponse>();
	              
	              for(String subBreed : subBreeds) {
	            	  DogResponse dg = new DogResponse();
	            	  dg.setBreed(subBreed);
	            	  dg.setSub_breed(new ArrayList<DogResponse>());
	            	  subBred.add(dg);
	              }
	              
	              dog.setSub_breed(subBred);
	              response.add(dog);
	          }

	      } catch (Exception e) {
	        // TODO: handle exception
	      }
	        
	        return response;
	    }

}
