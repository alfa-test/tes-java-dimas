package com.alfa.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfa.model.Employee;
import com.alfa.model.Salary;
import com.alfa.repository.EmployeeRepository;
import com.alfa.repository.RoleRepository;
import com.alfa.repository.SalaryRepository;
import com.alfa.request.EmployeeRequest;
import com.alfa.response.EmployeeResponse;
import com.alfa.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    SalaryRepository salaryRepository;

    @Override
    @Transactional
    public EmployeeResponse createEmployee(EmployeeRequest dto) {

        Employee employee = new Employee();
        if (null!=dto.getFullName())
            employee.setFullName(dto.getFullName());
        if (null!=dto.getAddress())
            employee.setAddress(dto.getAddress());
        if (null!=dto.getDob())
            employee.setDob(dto.getDob());
        if (null!=dto.getRoleId())
            employee.setRole(roleRepository.getOne(dto.getRoleId()));
        employeeRepository.save(employee);

        Salary salary = new Salary();
        if (null!=dto.getSalary())
            salary.setSalary(dto.getSalary());
        salaryRepository.save(salary);

        Employee savedEmployee = employeeRepository.findByFullNameAndAddressAndDobAndRole_Id(dto.getFullName(),dto.getAddress(),dto.getDob(),dto.getRoleId());
        Salary savedSalary = salaryRepository.getOne(savedEmployee.getId());

        return new EmployeeResponse(savedEmployee.getId(),savedEmployee.getFullName(),savedEmployee.getAddress(),savedEmployee.getDob(),savedEmployee.getRole().getId(),savedSalary.getSalary()) ;
    }

    @Override
    public List<EmployeeResponse>  getAllEmployee() {

        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeResponse> EmployeeResponseList = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeResponse saveEmployee = new EmployeeResponse();
            saveEmployee.setId(employee.getId());
            saveEmployee.setFullName(employee.getFullName());
            saveEmployee.setAddress(employee.getAddress());
            saveEmployee.setDob(employee.getDob());
            saveEmployee.setRoleId(employee.getRole().getId());
            Salary salary = salaryRepository.getOne(employee.getId());
            saveEmployee.setSalary(salary.getSalary());
            EmployeeResponseList.add(saveEmployee);
        }

        return EmployeeResponseList;
    }

    @Override
    public EmployeeResponse getEmployeeById(Integer id) {

        Employee employee = employeeRepository.getOne(id);
        Salary salary = salaryRepository.getOne(id);

        return new EmployeeResponse(employee.getId(),employee.getFullName(),employee.getAddress(),employee.getDob(),employee.getRole().getId(),salary.getSalary()) ;
    }

    @Override
    @Transactional
    public EmployeeResponse updateEmployee(Integer id, EmployeeRequest dto) {

        Employee employee = employeeRepository.getOne(id);
        if (null!=dto.getFullName())
            employee.setFullName(dto.getFullName());
        if (null!=dto.getAddress())
            employee.setAddress(dto.getAddress());
        if (null!=dto.getDob())
            employee.setDob(dto.getDob());
        if (null!=dto.getRoleId())
            employee.setRole(roleRepository.getOne(dto.getRoleId()));
        employeeRepository.save(employee);

        Salary salary = salaryRepository.getOne(id);
        if (null!=dto.getSalary())
            salary.setSalary(dto.getSalary());
        salaryRepository.save(salary);

        return new EmployeeResponse(id, employee.getFullName(),employee.getAddress(),employee.getDob(),employee.getRole().getId(),salary.getSalary());
    }

    @Override
    public String deleteEmployeeById(Integer id) {

        salaryRepository.deleteById(employeeRepository.getOne(id).getId());
        employeeRepository.deleteById(id);

        return "Employee "+id+" have been deleted succesfully";
    }
}
