package com.alfa.service;

import java.util.List;

import com.alfa.response.DogResponse;

public interface DogService {
	
	public List<DogResponse> getDogs();
	
}
