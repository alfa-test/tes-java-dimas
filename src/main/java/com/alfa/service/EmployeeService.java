package com.alfa.service;

import java.util.List;

import com.alfa.request.EmployeeRequest;
import com.alfa.response.EmployeeResponse;

public interface EmployeeService {

    EmployeeResponse createEmployee(EmployeeRequest dto);

    List<EmployeeResponse> getAllEmployee();

    EmployeeResponse getEmployeeById(Integer id);

    EmployeeResponse updateEmployee(Integer id, EmployeeRequest dto);

    String deleteEmployeeById(Integer id);

}
