package com.alfa.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alfa.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    public Employee findByFullNameAndAddressAndDobAndRole_Id(String fullName, String address, Date dob, Integer roleId);
}
