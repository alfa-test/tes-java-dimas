package com.alfa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alfa.model.Salary;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Integer> {
}
