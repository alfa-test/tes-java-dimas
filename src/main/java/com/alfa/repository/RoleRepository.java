package com.alfa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alfa.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
