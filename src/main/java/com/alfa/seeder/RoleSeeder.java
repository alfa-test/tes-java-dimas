package com.alfa.seeder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alfa.model.Employee;
import com.alfa.model.Role;
import com.alfa.model.Salary;
import com.alfa.repository.EmployeeRepository;
import com.alfa.repository.RoleRepository;
import com.alfa.repository.SalaryRepository;

@Component
public class RoleSeeder {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    SalaryRepository salaryRepository;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @PostConstruct
    public void seed() throws ParseException {
        seedRole();
        seedEmployee();
        seedSalary();
    }

    public void seedRole(){
        if (roleRepository.count()==0){
            Role role1 = new Role(1,"Manager");
            Role role2 = new Role(2,"Cashier");
            Role role3 = new Role(3,"Staff");
            Role role4 = new Role(4,"Receptionist");
            Role role5 = new Role(5,"Security");
            roleRepository.saveAll(Arrays.asList(role1,role2,role3,role4,role5));
        }
    }

    public void seedEmployee() throws ParseException {
        if (employeeRepository.count()==0){
            Employee employee1 = new Employee(1,"Adi Purnama", "Depok", dateFormat.parse("1985-02-20 16:00:22"), roleRepository.getOne(1));
            Employee employee2 = new Employee(2,"Wellsen", "Bogor", dateFormat.parse("1974-02-20 16:01:54"), roleRepository.getOne(5));
            Employee employee3 = new Employee(3,"ricky", "70 Cool Guy", dateFormat.parse("1983-07-01 00:00:00"), roleRepository.getOne(3));
            Employee employee4 = new Employee(4,"Akino Archillies", "21 Kino Kino", dateFormat.parse("2000-01-11 00:00:00"), roleRepository.getOne(3));
            Employee employee5 = new Employee(5,"Kevin Alexander", "34 Nishinoya", dateFormat.parse("1989-06-30 00:00:00"), roleRepository.getOne(2));
            Employee employee6 = new Employee(6,"Ivy Marcia", "06 Wall Street", dateFormat.parse("1997-09-12 00:00:00"), roleRepository.getOne(3));
            Employee employee7 = new Employee(7,"Martha Saphia", "78 Ruby Fort", dateFormat.parse("1988-03-03 00:00:00"), roleRepository.getOne(3));
            Employee employee8 = new Employee(8,"Nofiandy", "03 Smooth Hair", dateFormat.parse("1998-01-25 00:00:00"), roleRepository.getOne(4));
            Employee employee9 = new Employee(9,"Octaviany", "23 Relaxing Cafe", dateFormat.parse("1993-12-28 00:00:00"), roleRepository.getOne(5));
            employeeRepository.saveAll(Arrays.asList(employee1,employee2,employee3,employee4,employee5,employee6,employee7,employee8,employee9));
        }
    }

    public void seedSalary(){
        if (salaryRepository.count()==0){
            Salary salary1 = new Salary(1,employeeRepository.getOne(1),10000000);
            Salary salary2 = new Salary(2,employeeRepository.getOne(2),3500000);
            Salary salary3 = new Salary(3,employeeRepository.getOne(3),4570000);
            Salary salary4 = new Salary(4,employeeRepository.getOne(4),4040000);
            Salary salary5 = new Salary(5,employeeRepository.getOne(5),3770000);
            Salary salary6 = new Salary(6,employeeRepository.getOne(6),4770000);
            Salary salary7 = new Salary(7,employeeRepository.getOne(7),4510000);
            Salary salary8 = new Salary(8,employeeRepository.getOne(8),5240000);
            Salary salary9 = new Salary(9,employeeRepository.getOne(9),6670000);
            salaryRepository.saveAll(Arrays.asList(salary1,salary2,salary3,salary4,salary5,salary6,salary7,salary8,salary9));
        }
    }
}
